package server

import (
	"context"
	"fmt"

	"github.com/99designs/gqlgen/graphql"

	"git.sr.ht/~sircmpwn/core-go/auth"
)

func Admin(ctx context.Context, obj interface{},
	next graphql.Resolver) (interface{}, error) {

	if auth.ForContext(ctx).UserType != auth.USER_TYPE_ADMIN {
		return nil, fmt.Errorf("Access denied")
	}

	return next(ctx)
}

func AnonInternal(ctx context.Context, obj interface{},
	next graphql.Resolver) (interface{}, error) {

	if auth.ForContext(ctx).AuthMethod != auth.AUTH_ANON_INTERNAL {
		return nil, fmt.Errorf("Anonymous internal auth access denied")
	}

	return next(ctx)
}

func Internal(ctx context.Context, obj interface{},
	next graphql.Resolver) (interface{}, error) {

	if auth.ForContext(ctx).AuthMethod != auth.AUTH_INTERNAL {
		return nil, fmt.Errorf("Internal auth access denied")
	}

	return next(ctx)
}

func Private(ctx context.Context, obj interface{},
	next graphql.Resolver) (interface{}, error) {

	user := auth.ForContext(ctx)
	switch user.AuthMethod {
	case auth.AUTH_INTERNAL, auth.AUTH_COOKIE:
		return next(ctx)
	case auth.AUTH_OAUTH2:
		if user.BearerToken.ClientID != "" {
			return nil, fmt.Errorf("Private auth access denied")
		}
		return next(ctx)
	}

	return nil, fmt.Errorf("Private auth access denied")
}

func Access(ctx context.Context, obj interface{}, next graphql.Resolver,
	scope string, kind string) (interface{}, error) {

	if err := auth.ForContext(ctx).Access(scope, kind); err != nil {
		return nil, err
	}

	return next(ctx)
}
